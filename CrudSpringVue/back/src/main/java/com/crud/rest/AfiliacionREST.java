package com.crud.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.crud.dao.AfiliacionDAO;
import com.crud.entitys.Afiliacion;

@RestController
@RequestMapping("/")
public class AfiliacionREST {
	
	@Autowired
	private AfiliacionDAO afiliacionDao;
	
	@CrossOrigin(origins = "http://localhost:9000")
	//consultar todo
	@RequestMapping(value = "afiliacion", method = RequestMethod.GET)
	public ResponseEntity<List<Afiliacion>> getAfiliacion(){
		List<Afiliacion> afiliacion = afiliacionDao.findAll();
		return ResponseEntity.ok(afiliacion);
	}
	//consultar por id
	@RequestMapping(value = "afiliacion/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Afiliacion>> getAfiliacionById(@PathVariable("id") int afiliacionId){
		Optional<Afiliacion> afiliacion= afiliacionDao.findById(afiliacionId);		
		if(afiliacion.isPresent()) {
			return ResponseEntity.ok(afiliacion);
		}else {
			return ResponseEntity.noContent().build();
		}
	}
	//insertar
	@RequestMapping(value = "afiliacion", method = RequestMethod.POST)
	public ResponseEntity<Afiliacion> crearAfiliacion(@RequestBody Afiliacion afiliacion){
		Afiliacion newAfiliacion = afiliacionDao.save(afiliacion);
		return ResponseEntity.ok(newAfiliacion);
	}
	//eliminar
	@RequestMapping(value = "eliminarAfiliacion/{id}", method = RequestMethod.POST)
	public ResponseEntity<Void> eliminarById(@PathVariable("id") int afiliacionId){
		afiliacionDao.deleteById(afiliacionId);
		return ResponseEntity.ok(null);
	}
	//actualizar
	@RequestMapping(value = "actualizarAfiliacion/{id}",method = RequestMethod.POST)
	public ResponseEntity<Afiliacion> actualizarAfiliacion(@RequestBody Afiliacion afiliacion){
		Optional<Afiliacion> consultaAfiliacion= afiliacionDao.findById(afiliacion.getId());
		if(consultaAfiliacion.isPresent()) {
			Afiliacion newAfiliacion = consultaAfiliacion.get();
			newAfiliacion.setId(afiliacion.getId());
			newAfiliacion.setLider(afiliacion.getLider());
			newAfiliacion.setNombre_afiliacion(afiliacion.getNombre_afiliacion());
			newAfiliacion.setNombre_fundador(afiliacion.getNombre_fundador());
			newAfiliacion.setNombre_nacion(afiliacion.getNombre_nacion());
			afiliacionDao.save(newAfiliacion);
			return ResponseEntity.ok(newAfiliacion);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
//	@Bean
//	public WebMvcConfigurer corsConfigurer() {
//	        return new WebMvcConfigurer() {
//	                @Override
//	                public void addCorsMappings(CorsRegistry registry) {
//	                        registry.addMapping("afiliacion")
//	                                .allowedOrigins("http://localhost:9000")
//	                                .allowedMethods("GET", "POST", "PUT", "DELETE")
//	                                .maxAge(3600);
//	                }
//
//	        };
//	}

}
