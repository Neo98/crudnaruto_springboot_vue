package com.crud.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.crud.dao.PersonajesDAO;
import com.crud.entitys.Personaje;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class PersonajeREST {
	
	@Autowired
	private PersonajesDAO personajesDao;
	//consular todo
	@RequestMapping(value = "personaje", method = RequestMethod.GET)
	public ResponseEntity<List<Personaje>> getPersonaje(){
		List<Personaje> personajes = personajesDao.findAll();		
		return ResponseEntity.ok(personajes);
	}
	//consultar por id
	@RequestMapping(value = "personaje/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<Personaje>> getPersonajeById(@PathVariable("id") int personajeId){
		Optional<Personaje> personaje= personajesDao.findById(personajeId);		
		if(personaje.isPresent()) {
			return ResponseEntity.ok(personaje);
		}else {
			return ResponseEntity.noContent().build();
		}
	}
	//insertar
	@RequestMapping(value = "personaje", method = RequestMethod.POST)
	public ResponseEntity<Personaje> crearPersonaje(@RequestBody Personaje personaje){
		Personaje newPersonaje = personajesDao.save(personaje);
		return ResponseEntity.ok(newPersonaje);
	}
	//eliminar
	@RequestMapping(value = "eliminarPersonaje/{id}", method = RequestMethod.POST)
	public ResponseEntity<Void> eliminarById(@PathVariable("id") int personajeId){
		personajesDao.deleteById(personajeId);
		return ResponseEntity.ok(null);
	}
	//actualizar
	@RequestMapping(value = "actualizarPersonaje/{id}",method = RequestMethod.POST)
	public ResponseEntity<Personaje> actualizarPersonaje(@RequestBody Personaje personaje){
		Optional<Personaje> consultaPersonaje= personajesDao.findById(personaje.getId());
		if(consultaPersonaje.isPresent()) {
			Personaje newPersonaje = consultaPersonaje.get();
			newPersonaje.setId(personaje.getId());
			newPersonaje.setClan(personaje.getClan());
			newPersonaje.setEdad(personaje.getEdad());
			newPersonaje.setNombre(personaje.getNombre());
			newPersonaje.setRango(personaje.getRango());
			System.out.print(newPersonaje.toString());
			personajesDao.save(newPersonaje);
			return ResponseEntity.ok(newPersonaje);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@RequestMapping(value="hello", method=RequestMethod.GET)
	public String hello() {
		return "Hola mundo";
	}
	
}
