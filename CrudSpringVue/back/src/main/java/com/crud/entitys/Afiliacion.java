package com.crud.entitys;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "afiliacion")
public class Afiliacion {
	@Id
	@Column(name = "id_afiliacion")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column
	private String nombre_afiliacion;
	@Column
	private String nombre_fundador;
	@Column
	private String nombre_nacion;
	@Column
	private String lider;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "afiliacion")
	private List<Personaje> personajes;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre_afiliacion() {
		return nombre_afiliacion;
	}
	public void setNombre_afiliacion(String nombre_afiliacion) {
		this.nombre_afiliacion = nombre_afiliacion;
	}
	public String getNombre_fundador() {
		return nombre_fundador;
	}
	public void setNombre_fundador(String nombre_fundador) {
		this.nombre_fundador = nombre_fundador;
	}
	public String getNombre_nacion() {
		return nombre_nacion;
	}
	public void setNombre_nacion(String nombre_nacion) {
		this.nombre_nacion = nombre_nacion;
	}
	public String getLider() {
		return lider;
	}
	public void setLider(String lider) {
		this.lider = lider;
	}
	public List<Personaje> getPersonajes() {
		return personajes;
	}
	public void setPersonajes(List<Personaje> personajes) {
		this.personajes = personajes;
	}	
	

}
