package com.crud.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crud.entitys.Afiliacion;

@Repository
public interface AfiliacionDAO extends JpaRepository<Afiliacion, Serializable>{

}
