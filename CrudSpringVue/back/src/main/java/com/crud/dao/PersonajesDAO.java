package com.crud.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.crud.entitys.Personaje;

@Repository
public interface PersonajesDAO extends JpaRepository<Personaje, Serializable> {

}
